#!/usr/bin/env python
#encoding:utf-8

import xmlrpclib

from writeLog import logInfo



username = "admin"
pwd      = ""
dbname   = "erp"
serverIp = "yantai.starmerx.com"
port     = "6069"




##################### 所必须的导出的字段 ######################################
###  po号id   :也就是 External ID
###  订单明细ID: 
###  产品ID    :
###  
############################################################################

#======================== 应该把交易号和订单号回写到erp上面的 ========================
class ReWriteToErp():
	#def __init__(self,serverIp,port,dbname,username,pwd):
	def __init__(self):
		self.serverIp = serverIp
		self.port     = port
		self.dbname   = dbname
		self.username = username
		self.pwd      = pwd
		self.sock     = self.getSock()
		self.uid      = self.getUid()
	def getUid(self):
		dbname, username,pwd = self.dbname,self.username,self.pwd

		uid,sock = "",""
		sock_common = xmlrpclib.ServerProxy("http://%s:%s/xmlrpc/common"%(serverIp,port))
		try:
			uid = sock_common.login(dbname, username, pwd)
		except Exception,e:
			logInfo("Error,%s"%e)
		return uid

	def getSock(self):
		serverIp, port = self.serverIp, self.port	

		try:
			sock = xmlrpclib.ServerProxy("http://%s:%s/xmlrpc/object"%(serverIp,port))
		except Exception,e:
			logInfo("erp login Error,%s"%e)
		return sock
		

	def writeNotes(self,fieldNotes,order_id):
	    #回写交易号
		dbname, uid,pwd = self.dbname,self.uid,self.pwd		
		sock = self.sock
        #ids = 52477
		new_id = ""
		objectName = "purchase.order"
		try:
			new_id = sock.execute(dbname, uid, pwd, objectName, 'write', order_id, fieldNotes)
		except Exception,e:
			logInfo("Erp write notes Error,%s"%e)
		return new_id

	def writeUnit(self, fieldUnit, unit_id):
	    #修改单价
		dbname, uid,pwd = self.dbname,self.uid,self.pwd				
		ids , sock = unit_id, self.sock
		new_id = ""		
		objectNameUnit = "purchase.order.line"
		try:
			new_id = sock.execute(dbname, uid, pwd, objectNameUnit, 'write',ids, fieldUnit)
		except Exception,e:
			logInfo("Erp write unit Error,%s"%e)
		return new_id

	def createFare(self,fieldFare):
    	#创建运费
		dbname, uid,pwd,sock = self.dbname,self.uid,self.pwd, self.sock
		objectNameFare = "purchase.order.line"
		new_id = ""
		print fieldFare
		try:
			new_id = sock.execute(dbname, uid,pwd, objectNameFare, "create",fieldFare)
		except Exception,e:
			logInfo("Create Fare Error,%s"%e)
		return new_id
	

############################### 测试  ##################################

"""
order_id = 52408     #Ext ID
unit_id  = 80827     #订单明细ID
product_id = 2475    #产品运费的ID(固定)

fieldNotes = {
	"notes": "下面的text交易号、订单号",
	}

fieldUnit = {
	"price_unit":"12",
	}
fieldFare = {
	"name" : "names",
	#"order_id":"询价单id",
	"order_id": order_id,
	#"product_id": "产品id",
	"product_id": 2475,
	"product_qty": 1,
	"price_unit" : 22,
	"date_planned": "2015-07-11",
	"product_uom": 3,
	}
"""
#retoErp = ReWriteToErp()
#noteid = retoErp.writeNotes(fieldNotes,order_id)
#print noteid
#unitid = retoErp.writeUnit(fieldUnit,unit_id)
#print unitid
#fieldid = retoErp.createFare(fieldFare)
#print fieldid